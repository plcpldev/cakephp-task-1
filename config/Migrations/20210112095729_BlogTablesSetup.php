<?php
use Migrations\AbstractMigration;

class BlogTablesSetup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $articleCategoriesTable = $this->table('article_categories', ['collation'=>'utf8mb4_unicode_ci']);
        $articleCategoriesTable
            ->addColumn('name', 'string', [
                'limit' => 70
            ])
            ->addColumn('slug', 'string', [
                'limit' => 100
            ])
            ->addIndex(['slug'], [
                'unique' => true,
                'name' => 'idx_article_categories_unique'
            ])
            ->addColumn('status', 'smallinteger', [
                'default' => 1,
                'comment' => '0 - inactive, 1 - active'
            ])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP'
            ])
            ->addColumn('modified_at', 'datetime', [
                'null' => true,
                'default' => null,
                'update' => 'CURRENT_TIMESTAMP'
            ])
            ->create()
        ;

        $articlesTable = $this->table('articles', ['collation'=>'utf8mb4_unicode_ci']);
        $articlesTable
            ->addColumn('slug', 'string', [
                'limit' => 128,
                'null' => false
            ])
            ->addIndex(['slug'], [
                'unique' => true,
                'name' => 'idx_articles_unique'
            ])
            ->addColumn('status', 'smallinteger', [
                'default' => 0,
                'comment' => 'Statuses: -1 - deleted, 0 - inactive, 1 - active'
            ])
            ->addColumn('article_category_id', 'integer', [
                'null' => true,
                'default' => null
            ])
            ->addForeignKey('article_category_id', 'article_categories', 'id', [
                'delete' => 'SET_NULL',
                'update' => 'NO_ACTION'
            ])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP'
            ])
            ->addColumn('modified_at', 'datetime', [
                'null' => true,
                'default' => null,
                'update' => 'CURRENT_TIMESTAMP'
            ])
            ->addColumn('publish_at', 'datetime', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('author_full_name', 'string', [
                'limit' => 128,
                'null' => true,
                'default' => null
            ])
            ->addColumn('meta_title', 'string', [
                'limit' => 128,
                'null' => true,
                'default' => null
            ])
            ->addColumn('meta_description', 'string', [
                'limit' => 255,
                'null' => true,
                'default' => null
            ])
            ->addColumn('title', 'string', [
                'limit' => 255,
                'null' => true,
                'default' => null
            ])
            ->addColumn('short_content', 'text', [
                'null' => true,
                'default' => null,
                'comment' => 'Used to show an article preview'
            ])
            ->addColumn('content', 'text', [
                'null' => true,
                'default' => null
            ])
            ->create()
        ;
    }
}
