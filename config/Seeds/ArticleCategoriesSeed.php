<?php
use Migrations\AbstractSeed;

/**
 * ArticleCategories seed.
 */
class ArticleCategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Koronawirus',
                'slug' => 'koronawirus',
                'status' => '1',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
            [
                'id' => '2',
                'name' => 'Moda',
                'slug' => 'moda',
                'status' => '1',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
            [
                'id' => '3',
                'name' => 'Dom i ogród',
                'slug' => 'dom-i-ogrod',
                'status' => '1',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
            [
                'id' => '4',
                'name' => 'Porady',
                'slug' => 'porady',
                'status' => '1',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
            [
                'id' => '5',
                'name' => 'Inspiracje',
                'slug' => 'inspiracje',
                'status' => '1',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
            [
                'id' => '6',
                'name' => 'Oferty specjalne',
                'slug' => 'oferty-specjalne',
                'status' => '1',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
            [
                'id' => '7',
                'name' => 'Promocje',
                'slug' => 'promocje',
                'status' => '1',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
            [
                'id' => '8',
                'name' => 'Black Friday',
                'slug' => 'black-friday',
                'status' => '0',
                'created_at' => '2021-01-12 10:00:00',
                'modified_at' => NULL,
            ],
        ];

        $table = $this->table('article_categories');
        $table->insert($data)->save();
    }
}
